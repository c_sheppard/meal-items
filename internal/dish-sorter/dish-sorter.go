package dish_sorter

type DishSorter struct {
}

func New() *DishSorter {
	return &DishSorter{}
}

var suffix = "==== Just Falafs ====\n      ~ Menu ~\nStarters\n"

func (d *DishSorter) SortDishesConcurrent(starters, mains, desserts, chefDishes []string) string {
	// Disclaimer: Concurrency is overkill for this specific use case. The intention of this method is to demonstrate
	// the use of a particular facet of the Go programming language - as can be very useful in other more fitting use cases.

	startersChan := make(chan []string)
	mainsChan := make(chan []string)
	dessertsChan := make(chan []string)

	go rangeOverSlice(starters, chefDishes, startersChan)
	go rangeOverSlice(mains, chefDishes, mainsChan)
	go rangeOverSlice(desserts, chefDishes, dessertsChan)

	chefsStarters, chefsMains, chefsDesserts := <-startersChan, <-mainsChan, <-dessertsChan

	result := suffix + formatItems(chefsStarters) +
		"\nMains\n" + formatItems(chefsMains) + "\nDesserts\n" + formatItems(chefsDesserts)

	return result
}

func rangeOverSlice(menu, chefDishes []string, chefsDesserts chan []string) {
	var menuItems []string
	for i := range menu {
		for j := range chefDishes {
			if menu[i] == chefDishes[j] {
				menuItems = append(menuItems, menu[i])
				continue
			}
		}
	}
	chefsDesserts <- menuItems
}

func formatItems(chefsMenuItems []string) string {
	var result string
	for i := range chefsMenuItems {
		if len(chefsMenuItems) == 1 {
			result += chefsMenuItems[i]
			continue
		}
		if i != len(chefsMenuItems)-1 && i != 0 {
			result += ", "
		}
		if i == len(chefsMenuItems)-1 {
			result += " or "
		}
		result += chefsMenuItems[i]
	}

	return result
}

func (d *DishSorter) SortDishesSynchronous(starters, mains, desserts, chefDishes []string) string {
	chefsStarters := rangeOverSliceSync(starters, chefDishes)
	chefsMains := rangeOverSliceSync(mains, chefDishes)
	chefsDesserts := rangeOverSliceSync(desserts, chefDishes)

	result := suffix + formatItems(chefsStarters) + "\nMains\n" + formatItems(chefsMains) +
		"\nDesserts\n" + formatItems(chefsDesserts)

	return result
}

func rangeOverSliceSync(menu []string, chefDishes []string) []string {
	var menuItems []string
	for i := range menu {
		for j := range chefDishes {
			if menu[i] == chefDishes[j] {
				menuItems = append(menuItems, menu[i])
				continue
			}
		}
	}
	return menuItems
}