package dish_sorter_test

import (
	dish_sorter "meal-items/internal/dish-sorter"
	meal_categories "meal-items/internal/meal-categories"
	"testing"
)

var expectedOutput =
	`==== Just Falafs ====
      ~ Menu ~
Starters
dumplings, peanuts or salad
Mains
bento, sushi, pasta or rice
Desserts
apples, strawberries or cheese`

var foodCategories = map[string][]string{
	"starters": {
		"dumplings", "peanuts", "salad", "mash",
	},

	"mains": {
		"steak", "bento", "chicken", "pizza",
		"sandwich", "taco", "sushi", "burger",
		"hotdog", "pasta", "wrap", "rice",
	},

	"desserts": {
		"apples", "strawberries", "cheese",
		"icecream", "pie", "cake",
	},
}

var chefDishes = []string{
	"strawberries", "dumplings", "pasta",
	"rice", "apples", "salad", "peanuts",
	"cheese", "bento", "sushi",
}

func TestDishSorter_SortDishesConcurrent(t *testing.T) {

	tests := []struct {
		expectedOutput string
		foodCategories map[string][]string
		chefDishes     []string
	}{
		{
			expectedOutput: expectedOutput,
			foodCategories: foodCategories,
			chefDishes: chefDishes,
		},
	}

	for i := range tests {
		test := tests[i]
		dishSorter := dish_sorter.New()

		starters := test.foodCategories[meal_categories.Starters]
		mains := test.foodCategories[meal_categories.Mains]
		desserts := test.foodCategories[meal_categories.Desserts]

		result := dishSorter.SortDishesConcurrent(starters, mains, desserts, test.chefDishes)

		checkAndPrintIfErr(t, result, test, i)
	}
}

func TestDishSorter_SortDishesSynchronous(t *testing.T) {

	tests := []struct {
		expectedOutput string
		foodCategories map[string][]string
		chefDishes     []string
	}{
		{
			expectedOutput: expectedOutput,
			foodCategories: foodCategories,
			chefDishes: chefDishes,
		},
	}

	for i := range tests {
		test := tests[i]
		dishSorter := dish_sorter.New()

		starters := test.foodCategories[meal_categories.Starters]
		mains := test.foodCategories[meal_categories.Mains]
		desserts := test.foodCategories[meal_categories.Desserts]

		result := dishSorter.SortDishesSynchronous(starters, mains, desserts, test.chefDishes)

		checkAndPrintIfErr(t, result, test, i)
	}
}

func checkAndPrintIfErr(t *testing.T, result string, test struct {
	expectedOutput string
	foodCategories map[string][]string
	chefDishes     []string
}, i int) {
	if result != test.expectedOutput {
		t.Errorf("test %d: error", i)
		t.Errorf("expected: \n%s", test.expectedOutput)
		t.Errorf("got: \n%s", result)
	}
}